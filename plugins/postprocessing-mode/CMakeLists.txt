set(sources
  pqAEVAPostProcessingModeBehavior.cxx
  pqAEVAPostProcessingModeBehavior.h
)

set(rc_files
  pqAEVAPostProcessingModeBehavior.qrc
)

set(ui_files
  pqAEVAPostProcessingModeBehavior.ui
)

set(xml_files
  postprocessing-filters.xml
  postprocessing-sources.xml
)

set(interface)
paraview_plugin_add_action_group(
  CLASS_NAME pqAEVAPostProcessingModeBehavior
  GROUP_NAME "ToolBar/PostProcessing"
  INTERFACES group_interfaces
  SOURCES group_sources
)
list(APPEND interfaces ${group_interfaces})
list(APPEND sources ${group_sources})

# Now mush it all into a plugin:
paraview_add_plugin(
  aevaPostProcessingModePlugin
  FORCE_STATIC ON
  VERSION "1.0"
  UI_INTERFACES ${interfaces}
  UI_FILES ${ui_files}
  UI_RESOURCES ${rc_files}
  SOURCES ${sources}
)
target_link_libraries(
  aevaPostProcessingModePlugin
  LINK_PRIVATE
    ParaView::pqApplicationComponents
)
