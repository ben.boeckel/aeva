//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqAEVAPostProcessingModeBehavior.h"

// Client side
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqInterfaceTracker.h"
#include "pqOutputPort.h"
#include "pqPVApplicationCore.h"
#include "pqParaViewMenuBuilders.h"

// Qt
#include <QAction>
#include <QDir>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QMetaObject>
#include <QTimer>
#include <QToolBar>
#include <QWidget>

// Qt generated UI
#include "ui_pqAEVAPostProcessingModeBehavior.h"

#include <iostream>
#include <vector>

static pqAEVAPostProcessingModeBehavior* s_postMode = nullptr;

class pqAEVAPostProcessingModeBehavior::pqInternal
{
public:
  Ui::pqAEVAPostProcessingModeBehavior Actions;
  QAction* ModeAction;
  QWidget ActionsOwner;
};

pqAEVAPostProcessingModeBehavior::pqAEVAPostProcessingModeBehavior(QObject* parent)
  : Superclass(parent)
{
  m_p = new pqInternal;
  m_p->Actions.setupUi(&m_p->ActionsOwner);

  m_p->ModeAction = m_p->Actions.actionPostProcessingMode;

  if (!s_postMode)
  {
    s_postMode = this;
  }

  // By default, the button is off. Register them in this state.
  this->addAction(m_p->ModeAction);
  this->setExclusive(false);

  QObject::connect(this, SIGNAL(triggered(QAction*)), this, SLOT(switchModes(QAction*)));
  QTimer::singleShot(0, this, SLOT(prepare()));
}

pqAEVAPostProcessingModeBehavior::~pqAEVAPostProcessingModeBehavior()
{
  if (s_postMode == this)
  {
    s_postMode = nullptr;
  }

  delete m_p;
}

pqAEVAPostProcessingModeBehavior* pqAEVAPostProcessingModeBehavior::instance()
{
  return s_postMode;
}

void pqAEVAPostProcessingModeBehavior::prepare()
{
  QObject::connect(this, SIGNAL(togglePostProcessingMode(bool)), pqCoreUtilities::mainWidget(),
    SLOT(togglePostProcessingMode(bool)));
}

void pqAEVAPostProcessingModeBehavior::switchModes(QAction* a)
{
  emit togglePostProcessingMode(a->isChecked());
}
