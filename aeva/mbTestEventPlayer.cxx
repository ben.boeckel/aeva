//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "aeva/mbTestEventPlayer.h"
#include "aeva/mbTestData.h"

#include "pqCoreTestUtility.h"
#include "pqEventDispatcher.h"
#include "pqFileDialog.h"

#include <vtksys/SystemTools.hxx>

#include <QApplication>
#include <QtDebug>

mbTestEventPlayer::mbTestEventPlayer(QObject* p)
  : pqWidgetEventPlayer(p)
{
}

bool mbTestEventPlayer::playEvent(
  QObject* obj, const QString& cmd, const QString& args, bool& Error)
{
  // Handle playback for pqFileDialog and all its children ...
  pqFileDialog* object = nullptr;
  for (QObject* o = obj; o; o = o->parent())
  {
    object = qobject_cast<pqFileDialog*>(o);
    if (object)
    {
      break;
    }
  }
  if (!object)
  {
    return false;
  }

  QString fileString = args;

  const QString aeva_data_directory(aeva_data_dir);
  if (fileString.contains("$aeva_data_dir") && aeva_data_directory.isEmpty())
  {
    qCritical() << "You must set the $aeva_data_dir cmake variable to play-back file selections.";
    Error = true;
    return true;
  }

  const QString test_data_directory(aeva_test_data_dir);
  if (fileString.contains("$aeva_test_data_dir") && test_data_directory.isEmpty())
  {
    qCritical()
      << "You must set the $aeva_test_data_dir cmake variable to play-back file selections.";
    Error = true;
    return true;
  }

  const QString test_directory = pqCoreTestUtility::TestDirectory();
  if (fileString.contains("PARAVIEW_TEST_ROOT") && test_directory.isEmpty())
  {
    qCritical() << "You must specify --test-directory in the command line options.";
    Error = true;
    return true;
  }

  if (cmd == "filesSelected")
  {
    fileString.replace("$aeva_data_dir", aeva_data_directory);
    fileString.replace("$aeva_test_data_dir", test_data_directory);
    fileString.replace("$PARAVIEW_TEST_ROOT", test_directory);

    if (object->selectFile(fileString))
    {
      pqEventDispatcher::processEventsAndWait(0);
    }
    else
    {
      qCritical() << "Dialog couldn't accept " << fileString;
      Error = true;
    }

    return true;
  }

  if (cmd == "cancelled")
  {
    object->reject();
    return true;
  }
  if (cmd == "remove")
  {
    // Delete the file.
    fileString.replace("$aeva_data_dir", aeva_data_directory);
    fileString.replace("$aeva_test_data_dir", test_data_directory);
    fileString.replace("$PARAVIEW_TEST_ROOT", test_directory);
    vtksys::SystemTools::RemoveFile(fileString.toLatin1().data());
    return true;
  }

  qCritical() << "Unknown pqFileDialog command: " << obj << " " << cmd << " " << args;
  Error = true;
  return true;
}
