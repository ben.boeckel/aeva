//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "aeva/mbObjectFactory.h"
#include "vtkVersion.h"

// Include all of the classes for which we want to create overrides.

#include "aeva/mbReaderFactory.h"

vtkStandardNewMacro(mbObjectFactory);

// Now create the functions to create overrides with.

VTK_CREATE_CREATE_FUNCTION(mbReaderFactory);

mbObjectFactory::mbObjectFactory()
{
  this->RegisterOverride("vtkSMReaderFactory", "mbReaderFactory", "Override for ModelBuilder", 1,
    vtkObjectFactoryCreatembReaderFactory);
}

const char* mbObjectFactory::GetVTKSourceVersion()
{
  return VTK_SOURCE_VERSION;
}

void mbObjectFactory::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
