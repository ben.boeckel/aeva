//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef aeva_mbMenuBuilder_h
#define aeva_mbMenuBuilder_h

class QMenu;
class QWidget;
class QMainWindow;

class mbMenuBuilder
{
public:
  /**\brief Build the File menu.
    *
    */
  static void buildFileMenu(QMenu& menu);
};

#endif
