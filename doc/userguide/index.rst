================
aeva User's Guide
================

Computational Model Builder -- model builder or aeva for short --
is a customizable application to help you
(1) describe a simulation's input deck in enough detail that it can
be passed to a solver and
(2) create input files for a variety of solvers in different simulation
packages using your description.

This process can involve any or all of the following:

- importing a geometric model of the simulation domain or the domain's boundary;
- assigning sizing functions to specify mesh element size for analysis;
- submitting portions of the simulation domain to be meshed for analysis;
- assigning material properties to regions of the simulation domain;
- assigning boundary conditions to portions of the simulation domain's boundary;
- assigning initial conditions to portions of the simulation domain or its boundary; and
- assigning global simulation properties such as convergence criteria.

In order to do this, model builder needs a description of your simulation solver.
The description is a template which describes the simulation in a JSON or XML document,
plus a Python script that examines how the template has been filled out and writes
an input file for your simulation.
You can provide the template using the editor application that comes with aeva.
Once you have the template completed, you can use aeva to fill out the
template, associate it with a mesh, and submit it to run.

.. toctree::
   :maxdepth: 4
