![Aeva](./doc/userguide/figures/logo.png)

Introduction
============
The [Annotation Environment for Virtual Anatomy (aeva)][] is
an open-source, multi-platform simulation workflow framework based on
[Computational Model Builder (CMB)][], [ParaView][],
the [Visualization Toolkit (VTK)][VTK] and [Qt][].

The project
has grown through collaborative efforts between
the [Cleveland Clinic][], [Kitware Inc.][Kitware],
 and various other
government and commercial institutions, and acedemic partners.

[Annotation Environment for Virtual Anatomy (aeva)]: https://gitlab.kitware.com/aeva/aeva
[Computational Model Builder (CMB)]: https://www.ComputationalModelBuilder.org
[ParaView]: https://www.paraview.org
[QT]: https://www.qt.io
[VTK]: http://www.vtk.org
[Cleveland Clinic]: https://www.lerner.ccf.org/bme/
[Kitware]: https://www.kitware.com

Learning Resources
==================

As aeva is developed, we will post more information on using and developing with it.

Reporting Bugs
==============

If you have found a bug:

1. If you have a patch, please read the [CONTRIBUTING.md][] document.

2. Otherwise, please join a discussion on our [discourse site][] and ask
   about the expected and observed behaviors to determine if it is
   really a bug.

3. Finally, if the issue is not resolved by the above steps, open
   an entry in the [aeva issue tracker][].

[discourse site]: https://discourse.kitware.com/c/aeva
[aeva issue tracker]: https://gitlab.kitware.com/aeva/aeva/issues

Contributing
============

See [CONTRIBUTING.md][] for instructions to contribute.

[CONTRIBUTING.md]: CONTRIBUTING.md

Latest Release Notes
====================
Can be found [here](doc/release/aeva-0.1.md).

License
=======

Aeva is distributed under the OSI-approved BSD 3-clause License.
See [License.txt][] for details.

[License.txt]: LICENSE.txt
