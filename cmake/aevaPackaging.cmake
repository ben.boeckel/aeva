if (NOT WIN32 AND NOT APPLE)
  set(CPACK_SET_DESTDIR TRUE)
endif()
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Computational Model Builder")
set(CPACK_PACKAGE_VENDOR "Kitware")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt")
set(CPACK_PACKAGE_VERSION_MAJOR "${aeva_version_major}")
set(CPACK_PACKAGE_VERSION_MINOR "${aeva_version_minor}")
set(CPACK_PACKAGE_VERSION_PATCH "${aeva_version_patch}")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "aeva ${aeva_version}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "aeva-${aeva_version}")

if (NOT DEFINED CPACK_SYSTEM_NAME)
  set(CPACK_SYSTEM_NAME "${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}")
endif()
if (${CPACK_SYSTEM_NAME} MATCHES Windows)
  if (CMAKE_CL_64)
    set(CPACK_SYSTEM_NAME win64-${CMAKE_SYSTEM_PROCESSOR})
  else()
    set(CPACK_SYSTEM_NAME win32-${CMAKE_SYSTEM_PROCESSOR})
  endif(CMAKE_CL_64)
endif()

if (NOT DEFINED CPACK_PACKAGE_FILE_NAME)
  set(CPACK_PACKAGE_FILE_NAME "${CPACK_SOURCE_PACKAGE_FILE_NAME}-${CPACK_SYSTEM_NAME}")
endif()
set(CPACK_PACKAGE_CONTACT "kitware@kitware.com")

if (WIN32)
  # There is a bug in NSI that does not handle full unix paths properly. Make
  # sure there is at least one set of four (4) backlasshes.
  set(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\aeva.exe")
  set(CPACK_PACKAGE_EXECUTABLES
      "aeva" "Annotation and Exchange of Virtual Anatomy"
      "paraview" "ParaView"
  )
  set(CPACK_CREATE_DESKTOP_LINK_prototype 1)
  set(CPACK_NSIS_DISPLAY_NAME "${CPACK_PACKAGE_INSTALL_DIRECTORY}")
  set(CPACK_NSIS_HELP_LINK "http://www.computationalmodelbuilder.org/")
  set(CPACK_NSIS_URL_INFO_ABOUT "http://www.computationalmodelbuilder.org/")
  set(CPACK_NSIS_CONTACT ${CPACK_PACKAGE_CONTACT})
  set(CPACK_NSIS_MODIFY_PATH ON)
endif()

if (APPLE)
  set(CPACK_GENERATOR "DragNDrop")
endif()

configure_file("${CMAKE_CURRENT_SOURCE_DIR}/cmake/CPackOptions.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CPackOptions.cmake" @ONLY)
set(CPACK_PROJECT_CONFIG_FILE "${CMAKE_CURRENT_BINARY_DIR}/CPackOptions.cmake")

include(CPack)
