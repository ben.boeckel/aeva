# Provide an option for testing and add the copyright test.
# Also, verify that the git LFS data has been fetched properly and
# set CMake variables that point to data directories.

option(aeva_enable_testing "Build aeva Testing" ON)
if (aeva_enable_testing)
  enable_testing()
  include(CTest)

  set(aeva_test_dir ${aeva_BINARY_DIR}/testing/temporary)
  file(MAKE_DIRECTORY ${aeva_test_dir})

  unset(aeva_data_dir CACHE)
  file(READ "${CMAKE_CURRENT_SOURCE_DIR}/data/aeva-data" cdata)
  if (NOT cdata STREQUAL "\n")
    message(WARNING
      "Testing is enabled, but aeva's data is not available. Use git-lfs in order "
      "to obtain the testing data.")
    set(aeva_test_data_dir)
  else ()
    set(aeva_test_data_dir "${CMAKE_CURRENT_SOURCE_DIR}/data")
  endif ()

  #add the first test which is for checking the copyright
  add_test(NAME aeva-copyright-check
    COMMAND ${CMAKE_COMMAND}
      -D "aeva_SOURCE_DIR=${aeva_SOURCE_DIR}"
      -P "${CMAKE_CURRENT_SOURCE_DIR}/cmake/CheckCopyright.cmake"
  )
  set_tests_properties(aeva-copyright-check
    PROPERTIES LABELS "aeva;swprocess"
  )
endif()
